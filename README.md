# Minecraft 1.19 Enchanted Golden Apple recipe

Adds the enchanted golden apple recipe back to Minecraft 1.19.

## Getting started (Installation)

- Download the latest version of this datapack.
- Go to the single-player world selection screen.
- Click on 'Create New World'
- Click on 'Datapacks'
- Drag & Drop the EnchantedGoldenApple.zip file onto this screen.
- Move the datapacks from "Available" on the left, to "Selected" on the right (click on the arrow next to the datapack icon)

## Version compability

| Version         | Compatible |
| --------------- | ----------- |
| 1.19            | Yes         |
| Anything else   | No          |

## Recipe
![image.png](./image.png)
